			

			var markers = [];
			var branch;


			var orden = [];
			var address = getParameterByName('search');
			var slug = getParameterByName('slug');
			var distance = getParameterByName('distance');
			var lat = getParameterByName('o-lat');
			var lng = getParameterByName('o-lng');
		    var geocoder;
		    var zoom = [];
	    	var circles = [];
		    zoom[5] = 12;
		    zoom[10] = 11;
		    zoom[20] = 10;
		    zoom[50] = 9;

		    geocoder = new google.maps.Geocoder();
			var cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '', '', '']
			];

			// [Name, lat, lng, id, address 1, address 2, phone, distance, branch hours, Drive Thru Hours, Walk Up Hours, misc, photo]
			var locations = [
		      ['Coral Gables', 25.747484, -80.261882, 2, '2655 LeJeune Rd.', 'Coral Gables, FL 33134', '305-446-1919', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5 pm <br>Fri: 8 am - 6 pm', 'Mon - Thurs: 4 pm - 5 pm', '24-hour ATM', 'coral-gables.jpg', 'coral-gables'],
		      ['Doral', 25.797568, -80.353561, 3, '2500 NW 97th Ave.  Suite 100', 'Doral, FL 33172', '305-470-0102', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm<br>Fri: 8 am - 7 pm', '', '24-hour ATM', 'doral.jpg', 'doral'],
		      ['Downtown Ft. Lauderdale', 26.124551, -80.140425, 4, '200 N.E. 3rd Ave.', 'Fort Lauderdale, FL 33301', '954-519-0460', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5 pm<br>Fri: 8 am - 6 pm', '', '24-hour ATM', 'downtown-fort-lauderdale.jpg', 'downtown-fort-lauderdale'],
		      ['Palm Springs/Hialeah', 25.866350, -80.298480, 5, '790 West 49th St.', 'Hialeah, FL 33012', '305-825-5500', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'palm-springs-hialeah.jpg', 'palm-springs-hialeah'],
		      ['West Hialeah', 25.838152, -80.289578, 6, '1801 West 4th Ave.', 'Hialeah, FL 33010', '305-884-7400', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br>Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-hialeah.jpg', 'west-hialeah'],
		      ['Taft Street', 26.025973, -80.223253, 7, '6775 Taft St.', 'Hollywood, FL 33024', '954-983-1193', 0, 'Mon - Fri: 8:30 am - 6 pm <br>Sat: 9:00 am - 12 noon', '', '', '24-hour ATM', 'taft-street.jpg', 'taft-street'],
		      ['Main Office', 25.779036, -80.264440, 8, '780 N.W. 42nd Ave.', 'Miami, FL 33126', '305-446-9330', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'main-office.jpg', 'main-office'],
		      ['Airport West', 25.796605, -80.320400, 9, '7650 N.W. 25th St.', 'Miami, FL 33122', '305-593-5744', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br>Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', '', '24-hour ATM', 'airport-west.jpg', 'airport-west'],
		      ['Bird Road', 25.733647, -80.324879, 10, '7951 S.W. 40th St.', 'Miami, FL 33155', '305-266-9500', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm <br>', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'bird-road.jpg', 'bird-road'],
		      ['Brickell', 25.764096, -80.191804, 11, '1000 Brickell Ave.', 'Miami, FL 33131', '305-381-8555', 0, 'Mon - Thurs: 8:30 am - 4 pm <br> Fri: 8:30 am - 5 pm', '', '', '24-hour ATM', 'brickell.jpg', 'brickell'],
		      ['Coral Way', 25.746398, -80.388594, 12, '12005 S.W. 26th St.', 'Miami, FL 33175', '305-559-4466', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'coral-way.jpg', 'coral-way'],
		      ['Downtown Miami', 25.773338, -80.190124, 13, '165 S.E. First St.', 'Miami, FL 33131', '305-373-3379', 0, 'Mon - Thurs: 8:30 am - 4 pm <br>Fri: 8:30 am - 5 pm', '', '', '24-hour ATM', 'downtown-miami.jpg', 'downtown-miami'],
		      ['Kendall', 25.686353, -80.374115, 14, '10950 Kendall Dr.', 'Miami, FL 33176', '305-630-5100', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'kendall.jpg', 'kendall'],
		      ['Miller Drive', 25.713203, -80.431734, 15, '14651 S.W. 56th St.', 'Miami, FL 33175', '305-382-1032', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'miller-drive.jpg', 'miller-drive'],
		      ['Pinecrest', 25.644376, -80.332780, 16, '13593 S. Dixie Hwy.', 'Pinecrest, FL 33156', '305-595-6232', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'pinecrest.jpg', 'pinecrest'],
		      ['West Flagler', 25.768720, -80.337066, 17, '8700 West Flagler St.', 'Miami, FL 33174', '305-225-2522', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-flagler.jpg', 'west-flagler'],
		      ['Miami Beach', 25.813996, -80.129355, 18, '501 Arthur Godfrey Rd.', 'Miami Beach, FL 33140', '305-674-7443', 0, 'Mon - Thurs: 8:30 am - 4 pm <br> Fri: 8:30 am - 5 pm', '', 'Mon - Thurs: 4 pm - 5 pm', '24-hour ATM', 'miami-beach.jpg', 'miami-beach'],
		      ['Miami Lakes', 25.911058, -80.319953, 19, '7455 Miami Lakes Dr.', 'Miami Lakes, FL 33014', '305-512-0500', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'miami-lakes.jpg', 'miami-lakes'],
		      ['West Miami', 25.762955, -80.302682, 20, '6590 S.W. 8th St.', 'West Miami, FL 33144', '305-265-1025', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'west-miami.jpg', 'west-miami'],
		      ['Weston', 26.090905, -80.371116, 21, '2300 Weston Rd.', 'Weston, FL 33326', '954-384-2944 ', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8:30 am - 5 pm <br> Fri: 8:30 am - 6 pm', '', '24-hour ATM', 'weston.jpg', 'weston'],
		      ['Aventura', 25.96941,-80.1434745, 22, '20900 NE 30th Ave,', 'Aventura, FL 33180', '305-933-3223', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 6 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'aventura.jpg', 'aventura'],
		      ['West Kendall', 25.68393,-80.4471402, 23, '15680 S.W. 88th St.', 'Miami, FL 33196', '305-382-0424', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-kendall.jpg', 'west-kendall'],
		      ['South Miami', 25.7046235,-80.2874663, 24, '6939 Red Rd.', 'Coral Gables, FL 33143', '305-665-8041', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', '', '', '24-hour ATM', 'south-miami.jpg', 'south-miami']
		    ];

		    var default_center = new google.maps.LatLng(25.816720, -80.353383);
			var _center;
			var _startingPoint;
	    	var map; 
	    	var radio;


     	function getParameterByName(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}

		
		var opciones = {
		    enableHighAccuracy: true,
		    timeout: 10000,
		    maximumAge: 1000
		};


		
		

      	function searchBranch(slug) {
      		for (var i = 0; i < locations.length; i++) {
      			if (locations[i][13] == slug) {
      				branch = locations[i]
      			}
      		}
 		}

 		//InitMap();

 		function initBranch(slug, distance) {
 			searchBranch(slug);
			document.getElementById("photo").src = "img/photos/" + branch[12];

 			var _center = new google.maps.LatLng(branch[1], branch[2]);

	    	map = new google.maps.Map(document.getElementById('map'), {
		      zoom: 12,
		      center: _center,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

			var icon1 = "img/branchicons/marker.svg";

			marker = new google.maps.Marker({
				position: new google.maps.LatLng(branch[1], branch[2]),
				map: map, 
				icon: icon1
			});

			document.getElementById("branch-name").innerHTML = branch[0];
			document.getElementById("branch-name-mobile").innerHTML = branch[0];

			document.getElementById("button-map").href= "https://www.google.com/maps/dir/?api=1&origin="+lat+","+lng+"&destination="+branch[4]+" "+branch[5];
			document.getElementById("button-map-mobile").href= "https://www.google.com/maps/dir/?api=1&origin="+lat+","+lng+"&destination="+branch[4]+" "+branch[5];
			
			document.getElementById("branch-address").innerHTML = branch[4] + "<br>" + branch[5];
			document.getElementById("branch-address-mobile").innerHTML = branch[4] + " " + branch[5];

			document.getElementById("branch-phone").innerHTML = "<a class='linktel' href='tel:+1-" + branch[6] +"'>" + branch[6] +"</a>";
			
			document.getElementById("branch-phone-mobile").innerHTML = "<a class='linktel' href='tel:+1-" + branch[6] +"'>" + branch[6] +"</a>";
			document.getElementById("branch-phone-mobile-2").href = "tel:+1-"+branch[6]+"";

			document.getElementById("branch-hours").innerHTML = branch[8];

			document.getElementById("drive-hours").innerHTML = branch[9];

			document.getElementById("walk-hours").innerHTML = branch[10];

			document.getElementById("branch-distance").innerHTML = distance + " miles";

			if (branch[11] = '') {
				document.getElementById("branch-24atm").style.display="none";
			}

			ajustarTamaño();
 		}

 		function ajustarTamaño(){
		  var laimagen = document.getElementById("photo");
		  var elmapa = document.getElementById("map");
		  elmapa.style.height= laimagen.clientHeight + "px";
		}

		initBranch(slug, distance);

 		
 
		


	