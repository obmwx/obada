var now = new Date();
var days = new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
var months = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
var date = (
  (now.getDate() < 10)
  ? "0"
  : "") + now.getDate();
function fourdigits(number) {
  return (number < 1000)
    ? number + 1900
    : number;
}
today = days[now.getDay()] + ", " + date + " de " + months[now.getMonth()] + " de " + (
fourdigits(now.getYear()));
var date = document.getElementById('date').innerHTML = today;
