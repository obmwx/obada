			function getRatio() {
				var ratio = getParameterByName('ratio');
				if (ratio) {
					document.getElementById('miles').value = ratio
				} else {
					document.getElementById('miles').value = 5;
				};
				return ratio;
			}

			var markers = [];

			var radius = getRatio();

			var orden = [];
			var address = getParameterByName('search');
		    var geocoder;
		    var zoom = [];
	    	var circles = [];
		    zoom[5] = 12;
		    zoom[10] = 11;
		    zoom[20] = 10;
		    zoom[50] = 9;

		    geocoder = new google.maps.Geocoder();
			var cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '', '', '']
			];

			// [Name, lat, lng, id, address 1, address 2, phone, distance, branch hours, Drive Thru Hours, Walk Up Hours, misc, photo]
			var locations = [
		      ['Coral Gables', 25.747484, -80.261882, 2, '2655 LeJeune Rd.', 'Coral Gables, FL 33134', '305-446-1919', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5 pm <br>Vie: 8 am - 6 pm', 'Lun - Jue: 4 pm - 5 pm', 'Cajero Automático (ATM) – 24 horas', 'coral-gables.jpg', 'coral-gables'],
		      ['Doral', 25.797568, -80.353561, 3, '2500 NW 97th Ave.  Suite 100', 'Doral, FL 33172', '305-470-0102', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm<br>Vie: 8 am - 7 pm', '', 'Cajero Automático (ATM) – 24 horas', 'doral.jpg', 'doral'],
		      ['Downtown Ft. Lauderdale', 26.124551, -80.140425, 4, '200 N.E. 3rd Ave.', 'Fort Lauderdale, FL 33301', '954-519-0460', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5 pm<br>Vie: 8 am - 6 pm', '', 'Cajero Automático (ATM) – 24 horas', 'downtown-fort-lauderdale.jpg', 'downtown-fort-lauderdale'],
		      ['Palm Springs/Hialeah', 25.866350, -80.298480, 5, '790 West 49th St.', 'Hialeah, FL 33012', '305-825-5500', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br>Vie: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'palm-springs-hialeah.jpg', 'palm-springs-hialeah'],
		      ['West Hialeah', 25.838152, -80.289578, 6, '1801 West 4th Ave.', 'Hialeah, FL 33010', '305-884-7400', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br>Vie: 8 am - 7 pm <br>Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-hialeah.jpg', 'west-hialeah'],
		      ['Taft Street', 26.025973, -80.223253, 7, '6775 Taft St.', 'Hollywood, FL 33024', '954-983-1193', 0, 'Lun - Vie: 8:30 am - 6 pm <br>Sab: 9:00 am - 12 del mediodía', '', '', 'Cajero Automático (ATM) – 24 horas', 'taft-street.jpg', 'taft-street'],
		      ['Main Office', 25.779036, -80.264440, 8, '780 N.W. 42nd Ave.', 'Miami, FL 33126', '305-446-9330', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'main-office.jpg', 'main-office'],
		      ['Airport West', 25.796605, -80.320400, 9, '7650 N.W. 25th St.', 'Miami, FL 33122', '305-593-5744', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br>Vie: 8 am - 7 pm <br>Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'airport-west.jpg', 'airport-west'],
		      ['Bird Road', 25.733647, -80.324879, 10, '7951 S.W. 40th St.', 'Miami, FL 33155', '305-266-9500', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm <br>', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'bird-road.jpg', 'bird-road'],
		      ['Brickell', 25.764096, -80.191804, 11, '1000 Brickell Ave.', 'Miami, FL 33131', '305-381-8555', 0, 'Lun - Jue: 8:30 am - 4 pm <br> Vie: 8:30 am - 5 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'brickell.jpg', 'brickell'],
		      ['Coral Way', 25.746398, -80.388594, 12, '12005 S.W. 26th St.', 'Miami, FL 33175', '305-559-4466', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'coral-way.jpg', 'coral-way'],
		      ['Downtown Miami', 25.773338, -80.190124, 13, '165 S.E. First St.', 'Miami, FL 33131', '305-373-3379', 0, 'Lun - Jue: 8:30 am - 4 pm <br>Vie: 8:30 am - 5 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'downtown-miami.jpg', 'downtown-miami'],
		      ['Kendall', 25.686353, -80.374115, 14, '10950 Kendall Dr.', 'Miami, FL 33176', '305-630-5100', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'kendall.jpg', 'kendall'],
		      ['Miller Drive', 25.713203, -80.431734, 15, '14651 S.W. 56th St.', 'Miami, FL 33175', '305-382-1032', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'miller-drive.jpg', 'miller-drive'],
		      ['Pinecrest', 25.644376, -80.332780, 16, '13593 S. Dixie Hwy.', 'Pinecrest, FL 33156', '305-595-6232', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'pinecrest.jpg', 'pinecrest'],
		      ['West Flagler', 25.768720, -80.337066, 17, '8700 West Flagler St.', 'Miami, FL 33174', '305-225-2522', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-flagler.jpg', 'west-flagler'],
		      ['Miami Beach', 25.813996, -80.129355, 18, '501 Arthur Godfrey Rd.', 'Miami Beach, FL 33140', '305-674-7443', 0, 'Lun - Jue: 8:30 am - 4 pm <br> Vie: 8:30 am - 5 pm', '', 'Lun - Jue: 4 pm - 5 pm', 'Cajero Automático (ATM) – 24 horas', 'miami-beach.jpg', 'miami-beach'],
		      ['Miami Lakes', 25.911058, -80.319953, 19, '7455 Miami Lakes Dr.', 'Miami Lakes, FL 33014', '305-512-0500', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'miami-lakes.jpg', 'miami-lakes'],
		      ['West Miami', 25.762955, -80.302682, 20, '6590 S.W. 8th St.', 'West Miami, FL 33144', '305-265-1025', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'west-miami.jpg', 'west-miami'],
		      ['Weston', 26.090905, -80.371116, 21, '2300 Weston Rd.', 'Weston, FL 33326', '954-384-2944 ', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8:30 am - 5 pm <br> Vie: 8:30 am - 6 pm', '', 'Cajero Automático (ATM) – 24 horas', 'weston.jpg', 'weston'],
		      ['Aventura', 25.96941,-80.1434745, 22, '20900 NE 30th Ave,', 'Aventura, FL 33180', '305-933-3223', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 6 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'aventura.jpg', 'aventura'],
		      ['West Kendall', 25.68393,-80.4471402, 23, '15680 S.W. 88th St.', 'Miami, FL 33196', '305-382-0424', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-kendall.jpg', 'west-kendall'],
		      ['South Miami', 25.7046235,-80.2874663, 24, '6939 Red Rd.', 'Coral Gables, FL 33143', '305-665-8041', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'south-miami.jpg', 'south-miami']
		    ];

		    var default_center = new google.maps.LatLng(25.816720, -80.353383);
			var _center;
			var _startingPoint;
	    	var map; 
	    	var radio;


        function initMap(radio_millas) {
	    	map = new google.maps.Map(document.getElementById('map'), {
		      zoom: 12,
		      center: _center,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

	    	
			var input = document.getElementById('pac-input');
			var x = document.createElement("INPUT");
			x.setAttribute("type", "text");
			x.setAttribute("class", "theinput");
			x.setAttribute("placeholder", "Buscar Ciudad o Zona Postal");
			x.style.display = "block";
			var searchBox = new google.maps.places.SearchBox(x);
        	map.controls[google.maps.ControlPosition.TOP_LEFT].push(x);	
			//input.style.display = "block";

        	map.addListener('bounds_changed', function() {
          		searchBox.setBounds(map.getBounds());
        	});

        	codeAddress(radio_millas);
          		_center = new google.maps.LatLng(25.816720, -80.353383);

        	searchBox.addListener('places_changed', function() {
          		var places = searchBox.getPlaces();
          		if (places.length > 0) {
	          		_center = new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng());
		          		map.setCenter(_center);
	            		setLocations(document.getElementById('miles').value, map, locations, _center);
          		}
      		}); 
        	//document.getElementById("pac-input").style.display = "block"; 
      		setLocations(radio_millas, map, locations, _center);       	

        }
		function setMap(radio_millas) {
			setLocations(radio_millas, map, locations, _center);
		}
		function resetLocations() {
			//setMap(document.getElementById('miles').value);
			//clearMarkers();
			if (document.getElementById('miles').value <= 50) {
				map.setZoom(zoom[document.getElementById('miles').value]);
			} else {
				map.setZoom(5);
			};
			
			setLocations(document.getElementById('miles').value, map, locations, _center)
		}
		function setLocations(radio_millas, map, locations, _center) {
			clearMarkers();
			clearNears();
			radio = radio_millas * 1609.34;
		    var marker, i;
		    var x1;
		    var distancia;

		    

			//if (circle) {removeCircle()};
			//drawCircle (_center, radio);

			removeCircle();
			drawCircle (_center, radio);

		    for (i = 0; i < locations.length; i++) { 
		    	//var letter = String.fromCharCode("A".charCodeAt(0) + i);
		    	//alert(letter);

			    x1 = new google.maps.LatLng(locations[i][1], locations[i][2]);
				distancia = google.maps.geometry.spherical.computeDistanceBetween(x1, _center); 
							    
				updateNear(distancia, locations[i]);
				var condicion = (parseInt(distancia) < parseInt(radio));
				if (condicion) {
					locations[i][7] = distancia;
					orden.push(locations[i]);
			  	};
		    }

		    orden = burbuja(orden);
			
		    map.setZoom(zoom[radio_millas]);

			drawMarkers(orden);

		    if (markers.length > 0) {
		    	showNear(true);
		    } else {
		    	showNear(false);
		    };


		}
		function clearMarkers() {
			for(var i=0; i < markers.length; i++){
			    markers[i].setMap(null);
			}
		    markers = [];
		    cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '']
			];
			orden = [];
     	}
     	function updateNear(distancia, place) {
     		
     		if(distancia < cercanas[0][7]) {
     			cercanas[2] = cercanas[1];
     			cercanas[1] = cercanas[0];
     			cercanas[0] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13]];
     		} else if (distancia < cercanas[1][7]) {
     				cercanas[2] = cercanas[1];
     				cercanas[1] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13]];
     		} else if (distancia < cercanas[2][7]) {
     				cercanas[2] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13]];
     		};
     	}
     	function showNear(inRatio) {
     		document.getElementById("nearList").innerHTML = "";

     		_startingPoint = _center;
     		
     		var content = document.getElementById("nearList").innerHTML;
     		
     		if (inRatio) {
     			//document.getElementById("nearTitle").innerHTML = "The closest ones are";
	     		for (var i = 0; i < orden.length; i++) {
	     			var letter = String.fromCharCode("A".charCodeAt(0) + i);
    	 			var newTH = document.createElement('li');
    	 			newTH.setAttribute("class", "listBullet");
    	 			newTH.setAttribute("onclick", "goLocation2('"+ "branch.html?slug="+orden[i][13]+"&distance=" + (orden[i][7] / 1609.34).toFixed(2) +  "&lat="+_startingPoint.lat()+"&lng="+_startingPoint.lng() + "'" +")");
    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");
					var contentString = "<div class=w50>" +
						//"<a target='_blank' href='branch.html?slug="+cercanas[i][13]+"&distance=" + (cercanas[i][7] / 1609.34).toFixed(2) + "'>"
						"<img id='bullet" + i + "' src='img/bullets/bullet" + letter + ".png'></img>" + 
						"<img class=logo src=img/logoOB.svg>" + 
						"<label class=name><strong>"+orden[i][0]+"</strong></label>"+
						//"<a class='link-map' target=_blank href='https://maps.google.com/?q=@"+ orden[i][1] +","+ orden[i][2] +"'>" +
						//"<a class='link-map' target=_blank href='https://www.google.com/maps/dir/?api=1&origin="+_startingPoint.lat()+","+_startingPoint.lng()+"&destination="+orden[i][4]+" "+orden[i][5]+"'>" + 
						"<a class='link-map' target=_blank href='branch.html?slug="+orden[i][13]+"&distance=" + (orden[i][7] / 1609.34).toFixed(2) + "'>" + 
				        "<label><img class='address' src='img/location.svg'></img>"+orden[i][4]+"</label>"+" "+
				        "<label class=city>"+orden[i][5]+"</label><br>"+
				        "<label class=directions>Cómo llegar</label></a><br>" +
				        "<a class='link-map' target=_blank href=tel:" + orden[i][6] + ">" +
				        "<label><img class='phone' src='img/phone.svg'></img>"+orden[i][6]+"</label></a>" 
				        //"<br class='hide-mobile'>" +

				        if (orden[i][6] != '') {
				        	
				        	contentString = contentString +
				        	"<a class='link-map phone' target=_blank href=tel:" + orden[i][6] + ">Llamar</a><br>"
				        };

						contentString = contentString + 
				        "<br></div>" +
				        "<div class=w50>" + "<img class='branch-photo' src='img/photos/" + orden[i][12] + "'>" + "</div>";
				        contentString = contentString + "<div class='w50'>" + "<p class='p-left'>";

				        

				        if (orden[i][8] != '') {
				        	
				        	contentString = contentString +
				        	"<strong>Horario de Atención</strong><br>" +
				        	orden[i][8] + "<br><br>"
				        }

				        if (orden[i][10] != '') {
				    		contentString = contentString +
				        	"<strong>Horario de Ventanilla Exterior <br> para Peatones</strong><br>" +
				        	orden[i][10] + "<br class='hide-mobile'><br class='hide-mobile'>" 
				    	}

				    	contentString = contentString + "</p>";

				        contentString = contentString + "</div>";
				        contentString = contentString + "<div class='w50'> <p>";

						if (orden[i][9] != '') {
				        	contentString = contentString +
				        	"<strong>Horario de Ventanilla <br> para Automóviles</strong><br>" +
				        	orden[i][9] + "<br><br>" 
				    	} 	

				    	contentString = contentString +
				    	"<strong>" + orden[i][11] + "</strong>" +
				        "</p>" +
				        "</div>";

				        contentString = contentString + "<div class='w100'>" +
				        "<label><strong>Distancia: " + (orden[i][7] / 1609.34).toFixed(2) + " millas</strong></label>"
				        + "</div>";

				        
						newTH.innerHTML = contentString;

					document.getElementById("nearList").appendChild(newTH);
     		};

     		} else {
     			document.getElementById("nearTitle").innerHTML = "Estas son las sucursales más cercanas.";

     			for (var j = 0; j <= 2; j++) {
					if (cercanas[j][0] != '') {
						letter = String.fromCharCode("A".charCodeAt(0) + j);
						i = j;
						var newTH = document.createElement('li');
	    	 			newTH.setAttribute("onclick", "goLocation('"+ "branch.html?slug="+cercanas[j][13]+"&distance=" + (cercanas[j][7] / 1609.34).toFixed(2) +  "&lat="+_startingPoint.lat()+"&lng="+_startingPoint.lng() + "'" +")");
	    	 			newTH.setAttribute("class", "listBullet");
	    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
	    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");

	     				var contentString = "<div class=w50>" +
		     				"<img id='bullet" + i + "' src='img/bullets/bullet"+letter+".png'></img>" + 
	    	 				"<img class=logo src=img/logoOB.svg>" + 
							"<label class=name><strong>"+cercanas[j][0]+"</strong></label>"+
							//"<a class='link-map' target=_blank href='https://maps.google.com/?q=@"+ cercanas[j][1] +","+ cercanas[j][2] +"'>" +
							//"<a class='link-map' target=_blank href='https://www.google.com/maps/dir/?api=1&origin="+_startingPoint.lat()+","+_startingPoint.lng()+"&destination="+cercanas[j][4]+" "+cercanas[j][5]+"'>" + 
							"<a class='link-map' target=_blank href='branch.html?slug="+cercanas[j][13]+"&distance=" + (cercanas[j][7] / 1609.34).toFixed(2) + "'>" + 
					        	"<label><img class='address' src='img/location.svg'></img>"+cercanas[j][4]+"</label>"+" "+
					        	"<label class=city>"+cercanas[j][5]+"</label><br>"+
						        "<label class=directions>Cómo llegar</label></a><br>" +
						        "<a class='link-map' target=_blank href=tel:" + cercanas[j][6] + ">" +
					        	"<label><img class='phone' src='img/phone.svg'></img>"+cercanas[j][6]+"</label></a><br>";

					        	if (cercanas[j][6] != '') {
						        	
						        	contentString = contentString +
						        	"<a class='link-map phone' target=_blank href=tel:" + cercanas[j][6] + ">Llamar</a><br>"
						        };

						        contentString = contentString + 
					        "<br></div>"
					        	+
					        "<div class='w50'> <img class='branch-photo' src='img/photos/" + cercanas[j][12]+ "'> </div>" + 

					        "<div class='w50'>" + 
					        "<p class='p-left'>";
					            
					    	if (cercanas[j][8] != '') {
					        	
					        	contentString = contentString +
					        	"<strong>Horario de Atención</strong><br>" +
					        	cercanas[j][8] + "<br><br class='hide-mobile'>"
					        }
					     	
					    	if (cercanas[j][10] != '') {
					    		contentString = contentString +
					        	"<strong>Horario de Ventanilla Exterior <br> para Peatones</strong><br>" +
					        	cercanas[j][10] + "<br class='hide-mobile'>" 
					    	} 	


							contentString = contentString +
					        "</p><br class='hide-mobile'><br class='hide-mobile'>" +
					        "</div>" + "<div class='w50'><p>"; 

					        if (cercanas[j][9] != '') {
					        	contentString = contentString +
					        	"<strong>Horario de Ventanilla <br> para Automóviles</strong><br>" +
					        	cercanas[j][9] + "<br><br>" 
					    	} 

					        contentString = contentString + 
					        "<strong>" + cercanas[j][11] + "</strong></p></div>" +
					        "<div class='w100'>" + 
					        "<label><strong>Distancia: " + (cercanas[j][7] / 1609.34).toFixed(2) + " millas</strong></label>" +
					        "</div>";


				        
						newTH.innerHTML = contentString;

						document.getElementById("nearList").appendChild(newTH);
	     			};
     			};
				/*
				if (cercanas[0][0] != '') {
					letter = 'A';
					i = 0;
					var newTH = document.createElement('li');
					newTH.setAttribute("onclick", "goLocation(0)");
    	 			newTH.setAttribute("class", "listBullet");
    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");

     				var contentString = "<div class=w50>" +
	     				"<img id='bullet" + i + "' src='img/bullets/bullet"+letter+".png'></img>" + 
    	 				"<img class=logo src=img/logoOB.svg>" + 
						"<label class=name><strong>"+cercanas[0][0]+"</strong></label>"+
				        	"<label><img class='address' src='img/location.svg'></img>"+cercanas[0][4]+"</label><br>"+
				        	"<label class=city>"+cercanas[0][5]+"</label><br>"+
				        	"<label><img class='phone' src='img/phone.svg'></img>"+cercanas[0][6]+"</label><br>" +
				        "</div>"
				        	+
				        "<div class='w50'> <img src='img/photos/" + cercanas[0][12]+ "'> </div>" + 

				        "<div class='w50'>" + 
				        "<p class='p-left'>";
				            
				    	if (cercanas[0][8] != '') {
				        	
				        	contentString = contentString +
				        	"<strong>Branch Hours</strong><br>" +
				        	cercanas[0][8] + "<br><br>"
				        }
				     	
				    	if (cercanas[0][10] != '') {
				    		contentString = contentString +
				        	"<strong>Walk Up Hours</strong><br>" +
				        	cercanas[0][10] + "<br>" 
				    	} 	


						contentString = contentString +
				        "</p><br><br>" +
				        "</div>" + "<div class='w50'><p>"; 

				        if (cercanas[0][9] != '') {
				        	contentString = contentString +
				        	"<strong>Drive Thru Hours</strong><br>" +
				        	cercanas[0][9] + "<br><br>" 
				    	} 

				        contentString = contentString + 
				        "<strong>" + cercanas[0][11] + "</strong></p></div>" +
				        "<div class='w100'>" + 
				        "<label><strong>Distance: " + (cercanas[0][7] / 1609.34).toFixed(2) + " miles</strong></label>" +
				        "</div>";



					newTH.innerHTML = contentString;

					document.getElementById("nearList").appendChild(newTH);
     			};


     			if (cercanas[1][0] != '') {
	     			letter = 'B';
					i = 1;
					var newTH = document.createElement('li');
					newTH.setAttribute("onclick", "goLocation(1)");
    	 			newTH.setAttribute("class", "listBullet");
    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");
     				var contentString = "<div class=w50>" +
	     				"<img id='bullet" + i + "' src='img/bullets/bullet"+letter+".png'></img>" + 
    	 				"<img class=logo src=img/logoOB.svg>" + 
						"<label class=name><strong>"+cercanas[1][0]+"</strong></label>"+
				        	"<label><img class='address' src='img/location.svg'></img>"+cercanas[1][4]+"</label><br>"+
				        	"<label class=city>"+cercanas[1][5]+"</label><br>"+
				        	"<label><img class='phone' src='img/phone.svg'></img>"+cercanas[1][6]+"</label><br>" +
				        "</div>"
				        	+
				        "<div class='w50'> <img src='img/photos/" + cercanas[1][12]+ "'> </div>" + 

				        "<div class='w50'>" + 
				        "<p class='p-left'>";
				            
				    	if (cercanas[1][8] != '') {
				        	
				        	contentString = contentString +
				        	"<strong>Branch Hours</strong><br>" +
				        	cercanas[1][8] + "<br><br>"
				        }
				     	
				    	if (cercanas[1][10] != '') {
				    		contentString = contentString +
				        	"<strong>Walk Up Hours</strong><br>" +
				        	cercanas[1][10] + "<br>" 
				    	} 	


						contentString = contentString +
				        "</p><br><br>" +
				        "</div>" + "<div class='w50'><p>"; 

				        if (cercanas[1][9] != '') {
				        	contentString = contentString +
				        	"<strong>Drive Thru Hours</strong><br>" +
				        	cercanas[1][9] + "<br><br>" 
				    	} 

				        contentString = contentString + 
				        "<strong>" + cercanas[1][11] + "</strong></p></div>" +
				        "<div class='w100'>" + 
				        "<label><strong>Distance: " + (cercanas[1][7] / 1609.34).toFixed(2) + " miles</strong></label>" +
				        "</div>";


					newTH.innerHTML = contentString;

					document.getElementById("nearList").appendChild(newTH);
     			};


     			if (cercanas[2][0] != '') {
     				i= 2;
     				letter = 'C';
     				var newTH = document.createElement('li');
					newTH.setAttribute("onclick", "goLocation(2)");
    	 			newTH.setAttribute("class", "listBullet");
    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");
     			     				var contentString = "<div class=w50>" +
	     				"<img id='bullet" + i + "' src='img/bullets/bullet"+letter+".png'></img>" + 
    	 				"<img class=logo src=img/logoOB.svg>" + 
						"<label class=name><strong>"+cercanas[0][0]+"</strong></label>"+
				        	"<label><img class='address' src='img/location.svg'></img>"+cercanas[0][4]+"</label><br>"+
				        	"<label class=city>"+cercanas[0][5]+"</label><br>"+
				        	"<label><img class='phone' src='img/phone.svg'></img>"+cercanas[0][6]+"</label><br>" +
				        "</div>"
				        	+
				        "<div class='w50'> <img src='img/photos/" + cercanas[0][12]+ "'> </div>" + 

				        "<div class='w50'>" + 
				        "<p class='p-left'>";
				            
				    	if (cercanas[0][8] != '') {
				        	
				        	contentString = contentString +
				        	"<strong>Branch Hours</strong><br>" +
				        	cercanas[0][8] + "<br><br>"
				        }
				     	
				    	if (cercanas[0][10] != '') {
				    		contentString = contentString +
				        	"<strong>Walk Up Hours</strong><br>" +
				        	cercanas[0][10] + "<br>" 
				    	} 	


						contentString = contentString +
				        "</p><br><br>" +
				        "</div>" + "<div class='w50'><p>"; 

				        if (cercanas[0][9] != '') {
				        	contentString = contentString +
				        	"<strong>Drive Thru Hours</strong><br>" +
				        	cercanas[0][9] + "<br><br>" 
				    	} 

				        contentString = contentString + 
				        "<strong>" + cercanas[0][11] + "</strong></p></div>" +
				        "<div class='w100'>" + 
				        "<label><strong>Distance: " + (cercanas[0][7] / 1609.34).toFixed(2) + " miles</strong></label>" +
				        "</div>";
     			};
     			*/


     			//_center = new google.maps.LatLng(cercanas[0][1], cercanas[0][2]);
     			//map.setCenter(_center);
     			var z = ajustNearestZoom((cercanas[2][7] / 1609.34).toFixed(2));
     			if (z > 4) {
     				map.setZoom(zoom[z]);
     			} else {
     				map.setZoom(z);
     			};
     			
     			
     			setNearest3();
     		};
     	}
     	function ajustNearestZoom(distance) {
     		if (distance <= 5) {
     			return 5
     		} else if (distance <= 10) {
     			return 10
     		} else if (distance <= 20) {
     			return 20
     		}else if (distance <= 50) {
     			return 50
     		} else if (distance <= 200) {
     			return 4
     		} else {
     			return 3
     		};
     	}
     	function setNearest3() {
     		var infowindows = [];
			for (i = 0; i < cercanas.length ; i++) {
				var letter = String.fromCharCode("A".charCodeAt(0) + i);
				if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				var icon1 = "img/markers/marker" + letter + ".png";

				marker = new google.maps.Marker({
					position: new google.maps.LatLng(cercanas[i][1], cercanas[i][2]),
					map: map, 
					icon: icon1
				});
				
					
		    	var infowindow = new google.maps.InfoWindow();
		    	var contentString = "<img src=img/logoOB.svg>"+
		    		"<label><strong>"+cercanas[i][0]+"</strong></label><br>"+
		        	"<label>"+cercanas[i][4]+"</label><br>"+
		        	"<label>"+cercanas[i][5]+"</label><br>"+
		        	"<label>"+cercanas[i][6]+"</label><br>";
 	          	infowindow.setContent(contentString);
	          	infowindows.push(infowindow);
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				    return function() {
				    	letter = String.fromCharCode("A".charCodeAt(0) + i);
						if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				        infowindows[i].open(map, marker);
				        marker.setIcon("img/markers/marker" + letter + "2.png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + "2.png";
				    }
			    })(marker, i));	

				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
				    return function() {
				    	letter = String.fromCharCode("A".charCodeAt(0) + i);
    					if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				        infowindows[i].close(map, marker);
				        marker.setIcon("img/markers/marker" + letter + ".png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + ".png";
				    }
			    })(marker, i));	
		    markers.push(marker);
		    };
			//map.setZoom(10);
     	}
     	function goLocation(j) {
     		/* _center = new google.maps.LatLng(cercanas[j][1], cercanas[j][2]);
		     map.setCenter(_center);
		     if (document.getElementById('miles').value == 0) {
		     	document.getElementById('miles').value = 10;
		     };
		     setLocations(document.getElementById('miles').value, map, locations, _center);*/
		     window.open(j);
     	}
     	function goLocation2(j) {
     		/*_center = new google.maps.LatLng(orden[j][1], orden[j][2]);
		     map.setCenter(_center);
		     setLocations(document.getElementById('miles').value, map, locations, _center);*/
		     window.open(j)
     	}
     	function clearNears() {
     		document.getElementById("nearList").innerHTML = "";
     		document.getElementById("nearTitle").innerHTML = "";
     	}
     	function getParameterByName(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		function codeAddress(radio_millas) {
		    geocoder.geocode( { 'address': address}, function(results, status) {
		      if (status == 'OK') {
		        _center = results[0].geometry.location;
		        _startingPoint = _center;
		        map.setCenter(_center);
		        setLocations(document.getElementById('miles').value, map, locations, _center)
		      } else {
		        geoLocalitazion(radio_millas);
		      }
		    });
		}

		function mostrarErrores(error) {
		    switch (error.code) {
		        case error.PERMISSION_DENIED:
		            alert('Permiso denegado por el usuario'); 
		            break;
		        case error.POSITION_UNAVAILABLE:
		            alert('Posición no disponible');
		            break; 
		        case error.TIMEOUT:
		            alert('Tiempo de espera agotado');
		            break;
		        default:
		            alert('Error de Geolocalización desconocido :' + error.code);
		    }
		}

		var opciones = {
		    enableHighAccuracy: true,
		    timeout: 10000,
		    maximumAge: 1000
		};

		function mostrarPosicion(posicion) {
		    var latitud = posicion.coords.latitude;
		    var longitud = posicion.coords.longitude;
		    var precision = posicion.coords.accuracy;
		    var fecha = new Date(posicion.timestamp);
	    	alert('coordenadas: ' + latitud + ', ' + longitud);
			}

		function geoLocalitazion(radio_millas) {


/*if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(mostrarPosicion, mostrarErrores, opciones);    
} else {
    alert("Tu navegador no soporta la geolocalización, actualiza tu navegador.");
}*/




		  	//Geolocalización;
	        if (navigator.geolocation) {
	        	
	          navigator.geolocation.getCurrentPosition(function(position) {

		            _center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		            _startingPoint = _center;
		            map.setCenter(_center);
		            setLocations(radio_millas, map, locations, _center);
	          }, function() {
	          	mostrarErrores;
	            _center = default_center;
	            map.setCenter(_center);
	            setLocations(radio_millas, map, locations, _center);
	          }, opciones);
	        } else {
	          // Browser doesn't support Geolocation
	            _center = default_center;
	            _startingPoint = _center;
	            map.setCenter(_center);
	            setLocations(radio_millas, map, locations, _center);
	        };
	        _startingPoint = _center;	
		}
		
		function burbuja(miArray)
		{
			for(var i=1;i<miArray.length;i++)
			{
				for(var j=0;j<(miArray.length-i);j++)
				{
					if(miArray[j][7]>miArray[j+1][7])
					{
						k=miArray[j+1];
						miArray[j+1]=miArray[j];
						miArray[j]=k;
					}
				}
			}
			return miArray;
		}

		function drawMarkers(locations) {
			//var infowindow = new google.maps.InfoWindow();
			var infowindows = [];
			for (i = 0; i < locations.length ; i++) {
				var letter = String.fromCharCode("A".charCodeAt(0) + i);
				if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				var icon1 = "img/markers/marker" + letter + ".png";
				var icon2 = "img/markers/marker.png";

				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map, 
					icon: icon1
				});
				
					
		    	var infowindow = new google.maps.InfoWindow();
		    	var contentString = "<img src=img/logoOB.svg>"+
		    		"<label><strong>"+locations[i][0]+"</strong></label><br>"+
		        	"<label>"+locations[i][4]+"</label><br>"+
		        	"<label>"+locations[i][5]+"</label><br>"+
		        	"<label>"+locations[i][6]+"</label><br>";
 	          	infowindow.setContent(contentString);
	          	infowindows.push(infowindow);
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				    return function() {
				    	letter = String.fromCharCode("A".charCodeAt(0) + i);
						if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				        infowindows[i].open(map, marker);
				        marker.setIcon("img/markers/marker" + letter + "2.png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + "2.png";
				    }
			    })(marker, i));	

				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
				    return function() {
				    	letter = String.fromCharCode("A".charCodeAt(0) + i);
    					if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				        infowindows[i].close(map, marker);
				        marker.setIcon("img/markers/marker" + letter + ".png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + ".png";
				    }
			    })(marker, i));	
		    markers.push(marker);
		    };
		}

		function changeImage(i, letter) {

			document.getElementById("bullet"+i).src="img/bullets/bullet"+letter+"2.png";
			 markers[i].setIcon("img/markers/marker"+letter+"2.png");
			 markers[i].setMap(map);
		}

		function restoreImage(i, letter) {
			document.getElementById("bullet"+i).src="img/bullets/bullet"+letter+".png";
			markers[i].setIcon("img/markers/marker"+letter+".png");
			markers[i].setMap(map);
		}

		function drawCircle (center, ratio) {
			circle = new google.maps.Circle({
			      strokeColor: '#1081E0',
			      strokeOpacity: 0.5,
			      strokeWeight: 2,
			      fillColor: '#1081E0',
			      fillOpacity: 0.05,
			      map: map,
			      center: center,
			      radius: ratio
			    });
			circles.push(circle);
		}

		function removeCircle() {

			for (var i = 0; i < circles.length; i++) {
				circles[i].setMap(null);
			};
      	}

		initMap(radius);

	