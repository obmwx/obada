			var markers = [];
			var branch;


			var orden = [];
			var address = getParameterByName('search');
			var slug = getParameterByName('slug');
			var distance = getParameterByName('distance');
			var lat = getParameterByName('o-lat');
			var lng = getParameterByName('o-lng');
		    var geocoder;
		    var zoom = [];
	    	var circles = [];
		    zoom[5] = 12;
		    zoom[10] = 11;
		    zoom[20] = 10;
		    zoom[50] = 9;

		    geocoder = new google.maps.Geocoder();
			var cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '', '', '']
			];

			// [Name, lat, lng, id, address 1, address 2, phone, distance, branch hours, Drive Thru Hours, Walk Up Hours, misc, photo]
			var locations = [
		      ['Coral Gables', 25.747484, -80.261882, 2, '2655 LeJeune Rd.', 'Coral Gables, FL 33134', '305-446-1919', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5 pm <br>Vie: 8 am - 6 pm', 'Lun - Jue: 4 pm - 5 pm', 'Cajero Automático (ATM) – 24 horas', 'coral-gables.jpg', 'coral-gables', 'appt_coralgables', 0],
		      ['Doral', 25.797568, -80.353561, 3, '2500 NW 97th Ave.  Suite 100', 'Doral, FL 33172', '305-470-0102', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm<br>Vie: 8 am - 7 pm', '', 'Cajero Automático (ATM) – 24 horas', 'doral.jpg', 'doral', 'appt_doral', 0],
		      ['Downtown Ft. Lauderdale', 26.124551, -80.140425, 4, '200 N.E. 3rd Ave.', 'Fort Lauderdale, FL 33301', '954-519-0460', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5 pm<br>Vie: 8 am - 6 pm', '', 'Cajero Automático (ATM) – 24 horas', 'downtown-fort-lauderdale.jpg', 'downtown-fort-lauderdale', 'appt_downtownfortlauderdale', 0],
		      ['Palm Springs/Hialeah', 25.866350, -80.298480, 5, '790 West 49th St.', 'Hialeah, FL 33012', '305-825-5500', 0, 'Lun - Jue: 9 am - 4 pm<br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br>Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'palm-springs-hialeah.jpg', 'palm-springs-hialeah', 'appt_palmsrpringshialeah', 0],
		      ['West Hialeah', 25.838152, -80.289578, 6, '1801 West 4th Ave.', 'Hialeah, FL 33010', '305-884-7400', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br>Vie: 8 am - 7 pm <br>Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-hialeah.jpg', 'west-hialeah', 'appt_westhialeah', 0],
		      ['Taft Street', 26.025973, -80.223253, 7, '6775 Taft St.', 'Hollywood, FL 33024', '954-983-1193', 0, 'Lun - Vie: 8:30 am - 6 pm <br>Sab: 9:00 am - 12 del mediodía', '', '', 'Cajero Automático (ATM) – 24 horas', 'taft-street.jpg', 'taft-street', 'appt_taftstreet', 0],
		      ['Main Office', 25.779036, -80.264440, 8, '780 N.W. 42nd Ave.', 'Miami, FL 33126', '305-446-9330', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'main-office.jpg', 'main-office', 'appt_mainoffice', 0],
		      ['Airport West', 25.796605, -80.320400, 9, '7650 N.W. 25th St.', 'Miami, FL 33122', '305-593-5744', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br>Vie: 8 am - 7 pm <br>Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'airport-west.jpg', 'airport-west', 'appt_airportwest', 0],
		      ['Bird Road', 25.733647, -80.324879, 10, '7951 S.W. 40th St.', 'Miami, FL 33155', '305-266-9500', 0, 'Lun - Jue: 9 am - 4 pm <br>Vie: 9 am - 6 pm <br>', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'bird-road.jpg', 'bird-road', 'appt_birdroad', 0],
		      ['Brickell', 25.764096, -80.191804, 11, '1000 Brickell Ave.', 'Miami, FL 33131', '305-381-8555', 0, 'Lun - Jue: 8:30 am - 4 pm <br> Vie: 8:30 am - 5 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'brickell.jpg', 'brickell', 'appt_brickell', 0],
		      ['Coral Way', 25.746398, -80.388594, 12, '12005 S.W. 26th St.', 'Miami, FL 33175', '305-559-4466', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'coral-way.jpg', 'coral-way', 'appt_coralway', 0],
		      ['Downtown Miami', 25.773338, -80.190124, 13, '165 S.E. First St.', 'Miami, FL 33131', '305-373-3379', 0, 'Lun - Jue: 8:30 am - 4 pm <br>Vie: 8:30 am - 5 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'downtown-miami.jpg', 'downtown-miami', 'appt_downtownmiami', 0],
		      ['Kendall', 25.686353, -80.374115, 14, '10950 Kendall Dr.', 'Miami, FL 33176', '305-630-5100', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'kendall.jpg', 'kendall', 'appt_kendall', 0],
		      ['Miller Drive', 25.713203, -80.431734, 15, '14651 S.W. 56th St.', 'Miami, FL 33175', '305-382-1032', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'miller-drive.jpg', 'miller-drive', 'appt_millerdrive', 0],
		      ['Pinecrest', 25.644376, -80.332780, 16, '13593 S. Dixie Hwy.', 'Pinecrest, FL 33156', '305-595-6232', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'pinecrest.jpg', 'pinecrest', 'appt_pinecrest', 0],
		      ['West Flagler', 25.768720, -80.337066, 17, '8700 West Flagler St.', 'Miami, FL 33174', '305-225-2522', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-flagler.jpg', 'west-flagler', 'appt_westflagler', 0],
		      ['Miami Beach', 25.813996, -80.129355, 18, '501 Arthur Godfrey Rd.', 'Miami Beach, FL 33140', '305-674-7443', 0, 'Lun - Jue: 8:30 am - 4 pm <br> Vie: 8:30 am - 5 pm', '', 'Lun - Jue: 4 pm - 5 pm', 'Cajero Automático (ATM) – 24 horas', 'miami-beach.jpg', 'miami-beach', 'appt_miamibeach', 0],
		      ['Miami Lakes', 25.911058, -80.319953, 19, '7455 Miami Lakes Dr.', 'Miami Lakes, FL 33014', '305-512-0500', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'miami-lakes.jpg', 'miami-lakes', 'appt_miamilakes', 0],
		      ['West Miami', 25.762955, -80.302682, 20, '6590 S.W. 8th St.', 'West Miami, FL 33144', '305-265-1025', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', 'Lun - Vie: 8 am - 9 am', 'Cajero Automático (ATM) – 24 horas', 'west-miami.jpg', 'west-miami', 'appt_westmiami', 0],
		      ['Weston', 26.090905, -80.371116, 21, '2300 Weston Rd.', 'Weston, FL 33326', '954-384-2944 ', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8:30 am - 5 pm <br> Vie: 8:30 am - 6 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'weston.jpg', 'weston', 'appt_weston', 0],
		      ['Aventura', 25.96941,-80.1434745, 22, '20900 NE 30th Ave,', 'Aventura, FL 33180', '305-933-3223', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 6 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'aventura.jpg', 'aventura', 'appt_aventura', 0],
		      ['West Kendall', 25.68393,-80.4471402, 23, '15680 S.W. 88th St.', 'Miami, FL 33196', '305-382-0424', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', 'Lun - Jue: 8 am - 5:30 pm <br> Vie: 8 am - 7 pm <br> Sab: 9 am - 12 del mediodía', '', 'Cajero Automático (ATM) – 24 horas', 'west-kendall.jpg', 'west-kendall', 'appt_westkendall', 0],
		      ['South Miami', 25.7046235,-80.2874663, 24, '6939 Red Rd.', 'Coral Gables, FL 33143', '305-665-8041', 0, 'Lun - Jue: 9 am - 4 pm <br> Vie: 9 am - 6 pm', '', '', 'Cajero Automático (ATM) – 24 horas', 'south-miami.jpg', 'south-miami', 'appt_southmiami', 0],
		      ['Aloft Hotel', 25.747643,-80.2652909, 25, '2524 S. LeJeune Rd.', 'Coral Gables, FL 33134', '', 0, '', '', '', 'Sólo retiros en efectivo', 'atm.jpg', 'aloft-hotel', '', 1],
		      ['Florida International University', 25.7581159,-80.3837341, 26, '11200 S.W. 8th St.', 'Miami, FL 33174', '', 0, '', '', '', 'Cajero Automático (ATM) – 24 horas', 'atm.jpg', 'florida-international-university', '', 1],
		      ['Florida Memorial University', 25.9178966,-80.2705565, 27, '15800 N.W. 42nd Ave.', 'Opa Locka, FL 33054', '', 0, '', '', '', 'Cajero Automático (ATM) – 24 horas', 'atm.jpg', 'florida-memorial-university', '', 1],
		      ['Larkin Hospital', 25.7055392,-80.2953362, 28, '7031 S.W. 62nd Ave.', 'Miami, FL 33143', '', 0, '', '', '', 'Sólo retiros en efectivo', 'atm.jpg', 'larkin-hospital', '', 1],
		      ['Larkin Hospital / Palm Springs', 25.8690179,-80.3150122, 29, '1475 West 49th Place', 'Hialeah, FL 33012', '', 0, '', '', '', 'Sólo retiros en efectivo', 'atm.jpg', 'larkin-hospital-palm-springs', '', 1],
		      ['Silverspot Cinemas', 25.7714767,-80.1907572, 30, '300 S.E. 3rd St.', 'Miami, FL 33131', '', 0, '', '', '', 'Cajero Automático (ATM) – 24 horas', 'atm.jpg', 'silverspot-cinemas', '', 1],
		    ];

		    var default_center = new google.maps.LatLng(25.816720, -80.353383);
			var _center;
			var _startingPoint;
	    	var map; 
	    	var radio;

     	function getParameterByName(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}

		var opciones = {
		    enableHighAccuracy: true,
		    timeout: 10000,
		    maximumAge: 1000
		};

      	function searchBranch(slug) {
      		for (var i = 0; i < locations.length; i++) {
      			if (locations[i][13] == slug) {
      				branch = locations[i]
      			}
      		}
 		}
 		
 		function initBranch(slug, distance) {
 			searchBranch(slug);
			document.getElementById("photo").src = "img/photos/" + branch[12];
			
 			var _center = new google.maps.LatLng(branch[1], branch[2]);

	    	map = new google.maps.Map(document.getElementById('map'), {
		      zoom: 12,
		      center: _center,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

			var icon1 = "img/branchicons/marker.svg";

			marker = new google.maps.Marker({
				position: new google.maps.LatLng(branch[1], branch[2]),
				map: map, 
				icon: icon1
			});

			document.getElementById("branch-name").innerHTML = branch[0];
			document.getElementById("branch-name-mobile").innerHTML = branch[0];

			document.getElementById("button-map").href= "https://www.google.com/maps/dir/?api=1&origin="+lat+","+lng+"&destination="+branch[4]+" "+branch[5];
			document.getElementById("button-map-mobile").href= "https://www.google.com/maps/dir/?api=1&origin="+lat+","+lng+"&destination="+branch[4]+" "+branch[5];
			
			document.getElementById("button-appointment").href= "https://www.oceanbank.com/esp/"+branch[14]+".html";
			document.getElementById("button-appointment-mobile").href= "https://www.oceanbank.com/esp/"+branch[14]+".html";

			document.getElementById("branch-address").innerHTML = branch[4] + "<br>" + branch[5];
			document.getElementById("branch-address-mobile").innerHTML = branch[4] + " " + branch[5];

			if (branch[6] == '') {
				document.getElementById("branch-phone-title").classList.add("noshow");
				document.getElementById("branch-phone-mobile-title").classList.add("noshow");
				document.getElementById("branch-phone-mobile-2").classList.add("noshow");
			}

			document.getElementById("branch-phone").innerHTML = "<a class='linktel' href='tel:+1-" + branch[6] +"'>" + branch[6] +"</a>";
			
			document.getElementById("branch-phone-mobile").innerHTML = "<a class='linktel' href='tel:+1-" + branch[6] +"'>" + branch[6] +"</a>";
			document.getElementById("branch-phone-mobile-2").href = "tel:+1-"+branch[6]+"";

			if (branch[8] == '') {
				document.getElementById("branch-hours-title").classList.add("noshow");
			}
			document.getElementById("branch-hours").innerHTML = branch[8];

			if (branch[9] == '') {
				document.getElementById("drive-hours-title").classList.add("noshow");
			}
			document.getElementById("drive-hours").innerHTML = branch[9];

			if (branch[10] == '') {
				document.getElementById("walk-hours-title").classList.add("noshow");
			}
			document.getElementById("walk-hours").innerHTML = branch[10];

			document.getElementById("branch-distance").innerHTML = distance + " millas";

			if (branch[11] == '') {
				document.getElementById("branch-24atm-title").classList.add("noshow");
			} else {
				document.getElementById("branch-24atm").innerHTML = branch[11];
			}

			ajustarTamaño();
 		}

 		function ajustarTamaño(){
		  var laimagen = document.getElementById("photo");
		  var elmapa = document.getElementById("map");
		  elmapa.style.height= laimagen.clientHeight + "px";
		}

		initBranch(slug, distance);



	