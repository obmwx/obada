			function getRatio() {
				var ratio = getParameterByName('ratio');
				if (ratio) {
					document.getElementById('miles').value = ratio
				} else {
					document.getElementById('miles').value = 5;
				};
				return ratio;
			}

			var markers = [];

			var radius = getRatio();
			
			var orden = [];
			var address = getParameterByName('search');
		    var geocoder;
		    var zoom = [];
	    	var circles = [];
		    zoom[5] = 12;
		    zoom[10] = 11;
		    zoom[20] = 10;
		    zoom[50] = 9;

		    geocoder = new google.maps.Geocoder();
			var cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '', '', '']
			];

			// [Name, lat, lng, id, address 1, address 2, phone, distance, branch hours, Drive Thru Hours, Walk Up Hours, misc, photo, url-appointment, only atm]
			var locations = [
		      ['Coral Gables', 25.747484, -80.261882, 2, '2655 LeJeune Rd.', 'Coral Gables, FL 33134', '305-446-1919', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5 pm <br>Fri: 8 am - 6 pm', 'Mon - Thurs: 4 pm - 5 pm', '24-hour ATM', 'coral-gables.jpg', 'coral-gables', 'CoralGablesBranch2', 0],
		      ['Doral', 25.797568, -80.353561, 3, '2500 NW 97th Ave.  Suite 100', 'Doral, FL 33172', '305-470-0102', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm<br>Fri: 8 am - 7 pm', '', '24-hour ATM', 'doral.jpg', 'doral', 'DoralBranch', 0],
		      ['Downtown Ft. Lauderdale', 26.124551, -80.140425, 4, '200 N.E. 3rd Ave.', 'Fort Lauderdale, FL 33301', '954-519-0460', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5 pm<br>Fri: 8 am - 6 pm', '', '24-hour ATM', 'downtown-fort-lauderdale.jpg', 'downtown-fort-lauderdale', 'DowntownFtLauderdaleBranch', 0],
		      ['Palm Springs/Hialeah', 25.866350, -80.298480, 5, '790 West 49th St.', 'Hialeah, FL 33012', '305-825-5500', 0, 'Mon - Thurs: 9 am - 4 pm<br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'palm-springs-hialeah.jpg', 'palm-springs-hialeah', 'PalmSpringsHialeahBranch', 0],
		      ['West Hialeah', 25.838152, -80.289578, 6, '1801 West 4th Ave.', 'Hialeah, FL 33010', '305-884-7400', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br>Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-hialeah.jpg', 'west-hialeah', 'WestHialeahBranch1', 0],
		      ['Taft Street', 26.025973, -80.223253, 7, '6775 Taft St.', 'Hollywood, FL 33024', '954-983-1193', 0, 'Mon - Fri: 8:30 am - 6 pm <br>Sat: 9:00 am - 12 noon', '', '', '24-hour ATM', 'taft-street.jpg', 'taft-street', 'TaftStreetBranch', 0],
		      ['Main Office', 25.779036, -80.264440, 8, '780 N.W. 42nd Ave.', 'Miami, FL 33126', '305-446-9330', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'main-office.jpg', 'main-office', 'MainBranch', 0],
		      ['Airport West', 25.796605, -80.320400, 9, '7650 N.W. 25th St.', 'Miami, FL 33122', '305-593-5744', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br>Fri: 8 am - 7 pm <br>Sat: 9 am - 12 noon', '', '24-hour ATM', 'airport-west.jpg', 'airport-west', 'AirportWest', 0],
		      ['Bird Road', 25.733647, -80.324879, 10, '7951 S.W. 40th St.', 'Miami, FL 33155', '305-266-9500', 0, 'Mon - Thurs: 9 am - 4 pm <br>Fri: 9 am - 6 pm <br>', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'bird-road.jpg', 'bird-road', 'BirdRoad', 0],
		      ['Brickell', 25.764096, -80.191804, 11, '1000 Brickell Ave.', 'Miami, FL 33131', '305-381-8555', 0, 'Mon - Thurs: 8:30 am - 4 pm <br> Fri: 8:30 am - 5 pm', '', '', '24-hour ATM', 'brickell.jpg', 'brickell', 'CoralGablesBranch1', 0],
		      ['Coral Way', 25.746398, -80.388594, 12, '12005 S.W. 26th St.', 'Miami, FL 33175', '305-559-4466', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'coral-way.jpg', 'coral-way', 'CoralWay', 0],
		      ['Downtown Miami', 25.773338, -80.190124, 13, '165 S.E. First St.', 'Miami, FL 33131', '305-373-3379', 0, 'Mon - Thurs: 8:30 am - 4 pm <br>Fri: 8:30 am - 5 pm', '', '', '24-hour ATM', 'downtown-miami.jpg', 'downtown-miami', 'OceanBank1', 0],
		      ['Kendall', 25.686353, -80.374115, 14, '10950 Kendall Dr.', 'Miami, FL 33176', '305-630-5100', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'kendall.jpg', 'kendall', 'KendallBranch1', 0],
		      ['Miller Drive', 25.713203, -80.431734, 15, '14651 S.W. 56th St.', 'Miami, FL 33175', '305-382-1032', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'miller-drive.jpg', 'miller-drive', 'MillerDriveBranch', 0],
		      ['Pinecrest', 25.644376, -80.332780, 16, '13593 S. Dixie Hwy.', 'Pinecrest, FL 33156', '305-595-6232', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'pinecrest.jpg', 'pinecrest', 'PinecrestBranch', 0],
		      ['West Flagler', 25.768720, -80.337066, 17, '8700 West Flagler St.', 'Miami, FL 33174', '305-225-2522', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-flagler.jpg', 'west-flagler', 'WestFlaglerBranch', 0],
		      ['Miami Beach', 25.813996, -80.129355, 18, '501 Arthur Godfrey Rd.', 'Miami Beach, FL 33140', '305-674-7443', 0, 'Mon - Thurs: 8:30 am - 4 pm <br> Fri: 8:30 am - 5 pm', '', 'Mon - Thurs: 4 pm - 5 pm', '24-hour ATM', 'miami-beach.jpg', 'miami-beach', 'MiamiBeach', 0],
		      ['Miami Lakes', 25.911058, -80.319953, 19, '7455 Miami Lakes Dr.', 'Miami Lakes, FL 33014', '305-512-0500', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'miami-lakes.jpg', 'miami-lakes', 'MiamiLakesBranch1', 0],
		      ['West Miami', 25.762955, -80.302682, 20, '6590 S.W. 8th St.', 'West Miami, FL 33144', '305-265-1025', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', 'Mon - Fri: 8 am - 9 am', '24-hour ATM', 'west-miami.jpg', 'west-miami', 'WestMiamiBranck', 0],
		      ['Weston', 26.090905, -80.371116, 21, '2300 Weston Rd.', 'Weston, FL 33326', '954-384-2944 ', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8:30 am - 5 pm <br> Fri: 8:30 am - 6 pm', '', '24-hour ATM', 'weston.jpg', 'weston', 'WestonBranch', 0],
		      ['Aventura', 25.96941,-80.1434745, 22, '20900 NE 30th Ave,', 'Aventura, FL 33180', '305-933-3223', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 6 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'aventura.jpg', 'aventura', 'OceanBank', 0],
		      ['West Kendall', 25.68393,-80.4471402, 23, '15680 S.W. 88th St.', 'Miami, FL 33196', '305-382-0424', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', 'Mon - Thurs: 8 am - 5:30 pm <br> Fri: 8 am - 7 pm <br> Sat: 9 am - 12 noon', '', '24-hour ATM', 'west-kendall.jpg', 'west-kendall', 'WestKendallBranch', 0],
		      ['South Miami', 25.7046235,-80.2874663, 24, '6939 Red Rd.', 'Coral Gables, FL 33143', '305-665-8041', 0, 'Mon - Thurs: 9 am - 4 pm <br> Fri: 9 am - 6 pm', '', '', '24-hour ATM', 'south-miami.jpg', 'south-miami', 'SouthMiamiBranch1', 0],
		      ['Aloft Hotel', 25.747643,-80.2652909, 25, '2524 S. LeJeune Rd.', 'Coral Gables, FL 33134', '', 0, '', '', '', 'Cash withdrawals only', 'atm.jpg', 'aloft-hotel', '', 1],
		      ['Florida International University', 25.7581159,-80.3837341, 26, '11200 S.W. 8th St.', 'Miami, FL 33174', '', 0, '', '', '', '24-hour ATM', 'atm.jpg', 'florida-international-university', '', 1],
		      ['Florida Memorial University', 25.9178966,-80.2705565, 27, '15800 N.W. 42nd Ave.', 'Opa Locka, FL 33054', '', 0, '', '', '', '24-hour ATM', 'atm.jpg', 'florida-memorial-university ', '', 1],
		      ['Larkin Hospital', 25.7055392,-80.2953362, 28, '7031 S.W. 62nd Ave.', 'Miami, FL 33143', '', 0, '', '', '', 'Cash withdrawals only', 'atm.jpg', 'larkin-hospital', '', 1],
		      ['Larkin Hospital / Palm Springs', 25.8690179,-80.3150122, 29, '1475 West 49th Place', 'Hialeah, FL 33012', '', 0, '', '', '', 'Cash withdrawals only', 'atm.jpg', 'larkin-hospital-palm-springs', '', 1],
		      ['Silverspot Cinemas', 25.7714767,-80.1907572, 30, '300 S.E. 3rd St.', 'Miami, FL 33131', '', 0, '', '', '', '4-hour ATM', 'atm.jpg', 'silverspot-cinemas', '', 1] 
		    ];

		    var default_center = new google.maps.LatLng(25.816720, -80.353383);
			var _center;
			var _startingPoint;
	    	var map; 
	    	var radio;


        function initMap(radio_millas) {
	    	map = new google.maps.Map(document.getElementById('map'), {
		      zoom: 12,
		      center: _center,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

	    	
			var input = document.getElementById('pac-input');
			var x = document.createElement("INPUT");
			x.setAttribute("type", "text");
			x.setAttribute("class", "theinput");
			x.setAttribute("placeholder", "Search City or Zip Code");
			x.style.display = "block";
			var searchBox = new google.maps.places.SearchBox(x);
        	map.controls[google.maps.ControlPosition.TOP_LEFT].push(x);	

        	map.addListener('bounds_changed', function() {
          		searchBox.setBounds(map.getBounds());
        	});

        	codeAddress(radio_millas);
          		_center = new google.maps.LatLng(25.816720, -80.353383);

        	searchBox.addListener('places_changed', function() {
          		var places = searchBox.getPlaces();
          		if (places.length > 0) {
	          		_center = new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng());
		          		map.setCenter(_center);
	            		setLocations(document.getElementById('miles').value, map, locations, _center);
          		}
      		}); 
      		setLocations(radio_millas, map, locations, _center);       	

        }
		function setMap(radio_millas) {
			setLocations(radio_millas, map, locations, _center);
		}
		function resetLocations() {
			if (document.getElementById('miles').value <= 50) {
				map.setZoom(zoom[document.getElementById('miles').value]);
			} else {
				map.setZoom(5);
			};
			
			setLocations(document.getElementById('miles').value, map, locations, _center)
		}
		function setLocations(radio_millas, map, locations, _center) {
			clearMarkers();
			clearNears();
			radio = radio_millas * 1609.34;
		    var marker, i;
		    var x1;
		    var distancia;

			removeCircle();
			drawCircle (_center, radio);

		    for (i = 0; i < locations.length; i++) { 
			    x1 = new google.maps.LatLng(locations[i][1], locations[i][2]);
				distancia = google.maps.geometry.spherical.computeDistanceBetween(x1, _center); 
							    
				updateNear(distancia, locations[i]);
				var condicion = (parseInt(distancia) < parseInt(radio));
				if (condicion) {
					locations[i][7] = distancia;
					orden.push(locations[i]);
			  	};
		    }

		    orden = burbuja(orden);
			
		    map.setZoom(zoom[radio_millas]);

			drawMarkers(orden);

		    if (markers.length > 0) {
		    	showNear(true);
		    } else {
		    	showNear(false);
		    };


		}
		function clearMarkers() {
			for(var i=0; i < markers.length; i++){
			    markers[i].setMap(null);
			}
		    markers = [];
		    cercanas = [
				['', null, null, 0, '', '', '', 10000000000000, '', '', '', ''],
				['', null, null, 1, '', '', '', 10000000000000, '', '', '', ''],
				['', null, null, 2, '', '', '', 10000000000000, '', '', '', '']
			];
			orden = [];
     	}
     	function updateNear(distancia, place) {
     		
     		if(distancia < cercanas[0][7]) {
     			cercanas[2] = cercanas[1];
     			cercanas[1] = cercanas[0];
     			cercanas[0] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13], place[14]];
     		} else if (distancia < cercanas[1][7]) {
     				cercanas[2] = cercanas[1];
     				cercanas[1] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13], place[14]];
     		} else if (distancia < cercanas[2][7]) {
     				cercanas[2] = [place[0], place[1], place[2], place[3], place[4], place[5], place[6], distancia, place[8], place[9], place[10], place[11], place[12], place[13], place[14]];
     		};
     	}
     	function showNear(inRatio) {
     		document.getElementById("nearList").innerHTML = "";

     		_startingPoint = _center;

     		
     		var content = document.getElementById("nearList").innerHTML;
     		
     		if (inRatio) {
     			//document.getElementById("nearTitle").innerHTML = "The closest ones are";
	     		for (var i = 0; i < orden.length; i++) {
	     			letter = "";
	     			j = i; 
	     			if (j > 25) {
	     				j = j - 26;
	     				letter = "A"
	     			};	     			
	     			var letter = letter + String.fromCharCode("A".charCodeAt(0) + j);

    	 			var newTH = document.createElement('li');
    	 			newTH.setAttribute("class", "listBullet");
    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");
					var contentString = "<div class=w50>" +
						"<img id='bullet" + i + "' src='img/bullets/bullet" + letter + ".png' alt=''></img>" + 
						"<img class=logo src='img/logoOB.svg' alt=''>" + 
						"<label class=name><strong><a target=_blank href='branch.html?slug="+orden[i][13]+"&distance=" + (orden[i][7] / 1609.34).toFixed(2) + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+ "'>" + orden[i][0] + "</a></strong>";

						if (orden[i][15] == 1) {
				        	contentString = contentString + ' <img class="atm" src="img/atm.svg" alt=""></img>';
				        };

				        contentString = contentString + "</label>"+
						"<a class='link-map' target=_blank href='https://www.google.com/maps/dir/?api=1&origin="+_startingPoint.lat()+","+_startingPoint.lng()+"&destination="+orden[i][4]+" "+orden[i][5]+"'>" + 
				        "<label><img class='address' src='img/location.svg' alt=''></img>"+orden[i][4]+"</label>"+" "+
				        "<label class=city>"+orden[i][5]+"</label><br>"+
				        "<label class=directions>Directions</label></a><br>";

				        if (orden[i][6] != '') {
				        	
				        	contentString = contentString  +
				        	"<a class='link-map' target=_blank href=tel:" + orden[i][6] + ">"+
				        	"<label><img class='phone' src='img/phone.svg' alt=''></img>"+orden[i][6]+"</label></a>" +
				        	"<a class='link-map phone' target=_blank href=tel:" + orden[i][6] + ">Call Now</a><br>"
				        };

				        contentString = contentString + 
				        "<br></div>" +
				        "<div class=w50><a target=_blank href='branch.html?slug="+orden[i][13]+"&distance=" + (orden[i][7] / 1609.34).toFixed(2)  + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+ "'><img class='branch-photo' src='img/photos/" + orden[i][12] + "' alt=''></a></div>";
				        
				        contentString = contentString + "<div class='w50'>" + "<p class='p-left'>";

				        if (orden[i][8] != '') {
				        	
				        	contentString = contentString +
				        	"<strong>Branch Hours</strong><br>" +
				        	orden[i][8] + "<br><br>"
				        }

				        if (orden[i][10] != '') {
				    		contentString = contentString +
				        	"<strong>Walk Up Hours</strong><br>" +
				        	orden[i][10] + "<br class='hide-mobile'><br class='hide-mobile'>" 
				    	}

				    	contentString = contentString + "</p>";

				        contentString = contentString + "</div>";
				        contentString = contentString + "<div class='w50'> <p>";

						if (orden[i][9] != '') {
				        	contentString = contentString +
				        	"<strong>Drive Thru Hours</strong><br>" +
				        	orden[i][9] + "<br><br>" 
				    	} 	

				    	contentString = contentString +
				    	"<strong>" + orden[i][11] + "</strong>" +
				        "</p>" +
				        "</div><div class='w50'></div><div class='w50'><p><a target='_blank' href='branch.html?slug="+orden[i][13]+"&distance=" + (orden[i][7] / 1609.34).toFixed(2)  + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+ "' class='boton-details'>View Details<a/></p></div>";

				        contentString = contentString + "<div class='w50'>" +
				        "<label><strong>Distance: " + (orden[i][7] / 1609.34).toFixed(2) + " miles</strong></label>"
				        + "</div><div class='w50'>"; 


				        if (orden[i][14] != '') {
				        	contentString = contentString +
				        	"<p><a href='https://outlook.office365.com/owa/calendar/"+orden[i][14]+"@OceanBank.onmicrosoft.com/bookings/' target='blank' class='boton-details'>Make an Appointment<a/></p>";
				    	}

				    	contentString = contentString + "</div> ";

				        
						newTH.innerHTML = contentString;

					document.getElementById("nearList").appendChild(newTH);
     		};

     		} else {
     			document.getElementById("nearTitle").innerHTML = "These are your closest locations.";

     			for (var j = 0; j <= 2; j++) {
					if (cercanas[j][0] != '') {
						letter = String.fromCharCode("A".charCodeAt(0) + j);
						i = j;
						var newTH = document.createElement('li');
	    	 			newTH.setAttribute("class", "listBullet");
	    	 			newTH.setAttribute("onmouseenter", "changeImage("+i+", '"+letter+"')");
	    	 			newTH.setAttribute("onmouseleave", "restoreImage("+i+", '"+letter+"')");

	     				var contentString = "<div class=w50>" +
		     				"<img id='bullet" + i + "' src='img/bullets/bullet"+letter+".png' alt=''></img>" + 
	    	 				'<img class=logo src="img/logoOB.svg" alt="">' + 
							"<label class=name><strong><a target=_blank href='branch.html?slug="+cercanas[j][13]+"&distance=" + (cercanas[j][7] / 1609.34).toFixed(2) + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+  "'>" + cercanas[j][0] +"</a></strong>";

						if (cercanas[j][15] == 1) {
				        	contentString = contentString + ' <img class="atm" src="img/atm.svg" alt=""></img>';
				        };

				        contentString = contentString + 
					        "</label>" +
							"<a class='link-map' target=_blank href='https://www.google.com/maps/dir/?api=1&origin="+_startingPoint.lat()+","+_startingPoint.lng()+"&destination="+cercanas[j][4]+" "+cercanas[j][5]+"'>" + 
				        	"<label><img class='address' src='img/location.svg' alt=''></img>"+cercanas[j][4]+"</label>"+" "+
				        	"<label class=city>"+cercanas[j][5]+"</label><br>"+
					        "<label class=directions>Directions</label></a><br>";

					        if (cercanas[j][6] != '') {
						       	contentString = contentString  + 
							       	"<a class='link-map' target=_blank href=tel:" + cercanas[j][6] + ">" +
						        	"<label><img class='phone' src='img/phone.svg' alt=''></img>"+cercanas[j][6]+"</label></a>" +
							        "<a class='link-map phone' target=_blank href=tel:" + cercanas[j][6] + ">Call Now</a><br>"
						    };

					        contentString = contentString + 
					        "<br></div>"
					        	+
					        "<div class='w50'><a target=_blank href='branch.html?slug="+cercanas[j][13]+"&distance=" + (cercanas[j][7] / 1609.34).toFixed(2) + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+  "'><img class='branch-photo' src='img/photos/" + cercanas[j][12]+ "' alt=''></a> </div>" + 
					        "<div class='w50'>" + 
					        "<p class='p-left'>";
					            
					    	if (cercanas[j][8] != '') {
					        	
					        	contentString = contentString +
					        	"<strong>Branch Hours</strong><br>" +
					        	cercanas[j][8] + "<br><br class='hide-mobile'>"
					        }
					     	
					    	if (cercanas[j][10] != '') {
					    		contentString = contentString +
					        	"<strong>Walk Up Hours</strong><br>" +
					        	cercanas[j][10] + "<br class='hide-mobile'>" 
					    	} 	


							contentString = contentString +
					        "</p><br class='hide-mobile'><br class='hide-mobile'>" +
					        "</div>" + "<div class='w50'><p>"; 

					        if (cercanas[j][9] != '') {
					        	contentString = contentString +
					        	"<strong>Drive Thru Hours</strong><br>" +
					        	cercanas[j][9] + "<br><br>" 
					    	} 

					        contentString = contentString + 
					        "<strong>" + cercanas[j][11] + "</strong></p>" +
					        "</div><div class='w50'></div><div class='w50'><p><a target='_blank' href='branch.html?slug="+cercanas[j][13]+"&distance=" + (cercanas[j][7] / 1609.34).toFixed(2)  + "&o-lat="+_startingPoint.lat()+"&o-lng="+_startingPoint.lng()+ "' class='boton-details'>View Details<a/></p></div>";

					        contentString = contentString + 
					        "<div class='w50'>" + 
					        "<label><strong>Distance: " + (cercanas[j][7] / 1609.34).toFixed(2) + " miles</strong></label>" +
					        "</div><div class='w50'>";

					        if (cercanas[j][14] != '') {
    					    	contentString = contentString + 
    					    	"<p><a href='https://outlook.office365.com/owa/calendar/"+cercanas[j][14]+"@OceanBank.onmicrosoft.com/bookings/' target='blank' class='boton-details'>Make an Appointment<a/></p>"; 
    					    }

							contentString = contentString + "</div> ";


				        
						newTH.innerHTML = contentString;

						document.getElementById("nearList").appendChild(newTH);
	     			};
     			};
     			var z = ajustNearestZoom((cercanas[2][7] / 1609.34).toFixed(2));
     			if (z > 4) {
     				map.setZoom(zoom[z]);
     			} else {
     				map.setZoom(z);
     			};
     			
     			
     			setNearest3();
     		};
     	}
     	function ajustNearestZoom(distance) {
     		if (distance <= 5) {
     			return 5
     		} else if (distance <= 10) {
     			return 10
     		} else if (distance <= 20) {
     			return 20
     		}else if (distance <= 50) {
     			return 50
     		} else if (distance <= 200) {
     			return 4
     		} else {
     			return 3
     		};
     	}
     	function setNearest3() {
     		var infowindows = [];
			for (i = 0; i < cercanas.length ; i++) {
				var letter = String.fromCharCode("A".charCodeAt(0) + i);
				if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				var icon1 = "img/markers/marker" + letter + ".png";

				marker = new google.maps.Marker({
					position: new google.maps.LatLng(cercanas[i][1], cercanas[i][2]),
					map: map, 
					icon: icon1
				});
				
					
		    	var infowindow = new google.maps.InfoWindow();
		    	var contentString = "<img src='img/logoOB.svg' alt=''>"+
		    		"<label><strong>"+cercanas[i][0]+"</strong></label><br>"+
		        	"<label>"+cercanas[i][4]+"</label><br>"+
		        	"<label>"+cercanas[i][5]+"</label><br>"+
		        	"<label>"+cercanas[i][6]+"</label><br>";
 	          	infowindow.setContent(contentString);
	          	infowindows.push(infowindow);
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				    return function() {
				    	letter = "";
				    	j = i; 
				    	if (j > 25) {
				    		letter = "A";
				    		j = i - 26;
				    	};
						letter = letter + String.fromCharCode("A".charCodeAt(0) + j);
				        infowindows[i].open(map, marker);
				        marker.setIcon("img/markers/marker" + letter + "2.png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + "2.png";
				    }
			    })(marker, i));	

				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
				    return function() {
				    	letter = "";
				    	j = i; 
				    	if (j > 25) {
				    		letter = "A";
				    		j = i - 26;
				    	};
				    	letter = letter + String.fromCharCode("A".charCodeAt(0) + j);
				        infowindows[i].close(map, marker);
				        marker.setIcon("img/markers/marker" + letter + ".png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + ".png";
				    }
			    })(marker, i));	
		    markers.push(marker);
		    };
     	}
     	function goLocation(j) {
		     window.open(j);
     	}
     	function goLocation2(j) {
		     window.open(j)
     	}
     	function clearNears() {
     		document.getElementById("nearList").innerHTML = "";
     		document.getElementById("nearTitle").innerHTML = "";
     	}
     	function getParameterByName(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		function codeAddress(radio_millas) {
		    geocoder.geocode( { 'address': address}, function(results, status) {
		      if (status == 'OK') {
		        _center = results[0].geometry.location;
		        _startingPoint = _center;
		        map.setCenter(_center);
		        setLocations(document.getElementById('miles').value, map, locations, _center)
		      } else {
		        geoLocalitazion(radio_millas);
		      }
		    });
		}

		function mostrarErrores(error) {
		    switch (error.code) {
		        case error.PERMISSION_DENIED:
		            alert('Permiso denegado por el usuario'); 
		            break;
		        case error.POSITION_UNAVAILABLE:
		            alert('Posición no disponible');
		            break; 
		        case error.TIMEOUT:
		            alert('Tiempo de espera agotado');
		            break;
		        default:
		            alert('Error de Geolocalización desconocido :' + error.code);
		    }
		}

		var opciones = {
		    enableHighAccuracy: true,
		    timeout: 10000,
		    maximumAge: 1000
		};

		function mostrarPosicion(posicion) {
		    var latitud = posicion.coords.latitude;
		    var longitud = posicion.coords.longitude;
		    var precision = posicion.coords.accuracy;
		    var fecha = new Date(posicion.timestamp);
	    	alert('coordenadas: ' + latitud + ', ' + longitud);
			}

		function geoLocalitazion(radio_millas) {

		  	//Geolocalización;
	        if (navigator.geolocation) {
	        	
	          navigator.geolocation.getCurrentPosition(function(position) {

		            _center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		            _startingPoint = _center;
		            map.setCenter(_center);
		            setLocations(radio_millas, map, locations, _center);
	          }, function() {
	          	mostrarErrores;
	            _center = default_center;
	            map.setCenter(_center);
	            setLocations(radio_millas, map, locations, _center);
	          }, opciones);
	        } else {
	          // Browser doesn't support Geolocation
	            _center = default_center;
	            _startingPoint = _center;
	            map.setCenter(_center);
	            setLocations(radio_millas, map, locations, _center);
	        };
	        _startingPoint = _center;	
		}
		
		function burbuja(miArray)
		{
			for(var i=1;i<miArray.length;i++)
			{
				for(var j=0;j<(miArray.length-i);j++)
				{
					if(miArray[j][7]>miArray[j+1][7])
					{
						k=miArray[j+1];
						miArray[j+1]=miArray[j];
						miArray[j]=k;
					}
				}
			}
			return miArray;
		}

		function drawMarkers(locations) {
			var infowindows = [];
			for (i = 0; i < locations.length ; i++) {
				var letter = String.fromCharCode("A".charCodeAt(0) + i);
				if (String.fromCharCode(letter.charCodeAt(0)) > String.fromCharCode("Z".charCodeAt(0))) { letter = ""};
				var icon1 = "img/markers/marker" + letter + ".png";
				var icon2 = "img/markers/marker.png";

				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map, 
					icon: icon1
				});
				
					
		    	var infowindow = new google.maps.InfoWindow();
		    	var contentString = "<img src='img/logoOB.svg' alt=''> "+
		    		"<label><strong>"+locations[i][0]+"</strong></label><br>"+
		        	"<label>"+locations[i][4]+"</label><br>"+
		        	"<label>"+locations[i][5]+"</label><br>"+
		        	"<label>"+locations[i][6]+"</label><br>";
 	          	infowindow.setContent(contentString);
	          	infowindows.push(infowindow);
				google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				    return function() {
				    	letter = "";
				    	j = i; 
				    	if (j > 25) {
				    		letter = "A";
				    		j = i - 26;
				    	};
				    	letter = letter + String.fromCharCode("A".charCodeAt(0) + j);
				    	infowindows[i].open(map, marker);
				        marker.setIcon("img/markers/marker" + letter + "2.png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + "2.png";
				    }
			    })(marker, i));	

				google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
				    return function() {
				    	letter = "";
				    	j = i; 
				    	if (j > 25) {
				    		letter = "A";
				    		j = i - 26;
				    	};
				    	letter = letter + String.fromCharCode("A".charCodeAt(0) + j);
				        infowindows[i].close(map, marker);
				        marker.setIcon("img/markers/marker" + letter + ".png");
				        document.getElementById("bullet"+i).src="img/bullets/bullet" + letter + ".png";
				    }
			    })(marker, i));	
		    markers.push(marker);
		    };
		}

		function changeImage(i, letter) {

			document.getElementById("bullet"+i).src="img/bullets/bullet"+letter+"2.png";
			 markers[i].setIcon("img/markers/marker"+letter+"2.png");
			 markers[i].setMap(map);
		}

		function restoreImage(i, letter) {
			document.getElementById("bullet"+i).src="img/bullets/bullet"+letter+".png";
			markers[i].setIcon("img/markers/marker"+letter+".png");
			markers[i].setMap(map);
		}

		function drawCircle (center, ratio) {
			circle = new google.maps.Circle({
			      strokeColor: '#1081E0',
			      strokeOpacity: 0.5,
			      strokeWeight: 2,
			      fillColor: '#1081E0',
			      fillOpacity: 0.05,
			      map: map,
			      center: center,
			      radius: ratio
			    });
			circles.push(circle);
		}

		function removeCircle() {

			for (var i = 0; i < circles.length; i++) {
				circles[i].setMap(null);
			};
      	}

		initMap(radius);

function openList() {
	$( ".select" ).toggleClass( "open" )
}

$("#miles").focusout(function(){
  $( ".select" ).removeClass('open');
});



		$('#miles').change(function() {
			var sel = document.getElementById('miles');
			//var val = sel.options[sel.selectedIndex].value;
			var valor = sel.value;
			var val = sel.value;
  			var opts = sel.options;
  			for (var opt, j = 0; opt = opts[j]; j++) {
    		if (opt.value == val) {
      			opt.setAttribute("selected", "true");
      			opt.setAttribute("aria-selected", "true");
      			} else {
      				opt.setAttribute("selected", "false");
      				opt.setAttribute("aria-selected", "false");
      			}
    		}


    				sel.value = valor;

  			
		});	

		// A $( document ).ready() block.


				function setAria() {
					var sel = document.getElementById('miles');
		  			var opt = sel.options[sel.selectedIndex];
	      			opt.setAttribute("selected", "true");
	      			opt.setAttribute("aria-selected", "true");
		    	};

				$( document ).ready(function() {
				      setAria();
				});