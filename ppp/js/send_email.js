


$('.submit-button').on('click', function(event) {
    event.preventDefault();
    var formValid = true;
    
      $('.validate').each(function(){
        formValid = validate($(this).attr('id')) && formValid;
      });
      if (formValid) {
        var formData = $('.form').serialize();
        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $('.form').attr('action'),
            data: formData
        })
        .done(function(response) {
          if (response == "OK") {
            $( '.form' ).slideUp( "slow", function() {
            // Animation complete.
            });
            $('.w-form-fail').removeClass('visible');
            $('.w-form-error').removeClass('visible');
            $('.w-form-done').addClass('visible');
          } else  {
            $('.w-form-error').removeClass('visible');
            $('.w-form-fail').addClass('visible');
          }
        })
        .fail(function(data) {
          $('.w-form-error').removeClass('visible');
          $('.w-form-fail').addClass('visible');
        });
      } else {
        
        $('.w-form-error').addClass('visible');
      };
   
});

function validate (id) {
  var elem=document.getElementById(id);
  var valid = false;
  if (elem.checkValidity()) {

    if ($('#'+id).prop("tagName") != 'SELECT') {
      $('#'+id).removeClass('invalid');
    } else {
      $('#'+id).next().removeClass('invalid');
    }
    valid=true;
  } else {
    if ($('#'+id).prop("tagName") != 'SELECT') {
      $('#'+id).addClass('invalid');
    } else {
      $('#'+id).next().addClass('invalid');
    }
    valid=false;
  }
  return valid;
}

$('.validate').on('change', function () {
  
  if ($(this).prop("tagName") != 'DIV') {
    validate($(this).attr('id')); 
  }
});